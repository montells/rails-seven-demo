# README

Rails 7 demo from [# Rails 7: The Demo by DHH](https://www.youtube.com/watch?v=mpWFrUwAN88&t=1251s)


- rails _7.0.0_ new
- posts scaffold
- css from cdn
- model validations
- rich_text
- active_storage
- adding js via importmaps
- adding emails
- adding action_cable broadcasts
- switch to posgtres
- deploy on heroku